//nomor 1
  const golden = goldenFunction => {
      console.log("this is golden!!")
  }
  golden ();

  //nomor 2
  const firstname = 'William'
  const lastname = 'Imoh'
  const william = `${firstname} ${lastname}`
  console.log (william)


//nomor 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }
 const {firstName,lastName,destination,occupation,spell} = newObject;
 console.log(firstName, lastName, destination, occupation);

 //nomor 4
 let west = ["Will", "Chris", "Sam", "Holly"]
 let east = ["Gill", "Brian", "Noel", "Maggie"]
 let combined = [...west,...east]
 console.log (combined)

 //nomor 5
const planet = "earth"
const view = "glass"
const before = `lorem ${view} dolor sit amet  
consectetur adipiscing elit planet do eiusmod tempor
incididunt ut labore et dolore magna aliqua. Ut enim
 ad minim veniam`
// Driver Code
console.log(before) 