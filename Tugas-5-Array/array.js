//Soal 1

function range (startNum, finishNum){
    var rangeArr = [];
    if (startNum > finishNum){
        var rangeLength = startNum - finishNum + 1;
        for (var i = 0; i< rangeLength; i++){
            rangeArr.push(startNum - i)
        }
    }else if (startNum < finishNum ){
        var rangeLength = finishNum - startNum +1;
        for (var i = 0 ; i <rangeLength;i++){
            rangeArr.push(startNum+i)
        }
    }else if ( !startNum || ! finishNum){
        return -1
    }
    return rangeArr
}
console.log(range(1, 10))
console.log(range(1)) 
console.log(range(11, 19)) 
console.log(range(54, 50))
console.log(range()) 


//nomor 2
function rangeWithStep (startNum, finishNum,step){
    var rangeArr = [];

    if (startNum > finishNum){
        var currentNum = startNum;
        for (var i = 0; currentNum>= finishNum; i++){
            rangeArr.push(currentNum)
            currentNum -=step
        }
    }else if (startNum < finishNum ){
        var currentNum = startNum;
        for (var i = 0 ; currentNum<= finishNum;i++){
            rangeArr.push(currentNum)
            currentNum +=step
        }
    }else if ( !startNum || ! finishNum || !step){
        return -1
    }
    return rangeArr
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//nomor 3
function sum (startnum,finishnum,step){
    var number = [];
    if (startnum>finishnum){
        var rentang = startnum-finishnum+1
        for (var i=0;i<rentang;i+=step){
            number.push (startnum - i)
        }
    }
    else{
        var rentang = finishnum - startnum+1
        for (var i = 0; i<rentang;i+=step){
            number.push (startnum+i)
        }
    }
    var jumlah =0;
    for (var i=0;i<number.length;i++){
        jumlah+=number[i]
    }
    return jumlah;
}
console.log (sum(5,50,2))

//saya hanya mampu mengerjakan 3 soal ini dan menurut saya array cukup sulit
//semoga saya bisa mengerti penjelasan soal ini nantinya