var nama = "";
var peran = "";
if (nama == "John" && peran == "Guard"){
    console.log ("Selamat datang di Dunia Werewolf, " + nama);
    console.log ("Halo " + peran + nama + ", Kamu akan membantu melindungi temanmu dari serangan werewolf");
}else if (nama == "John" && peran == ""){
    console.log ("Halo " + nama + ", Pilih Peranmu untuk memulai game!");
}else {
    console.log ("Nama harus diisi!");
}
/*
// Output untuk Input nama = '' dan peran = ''
"Nama harus diisi!"
 
//Output untuk Input nama = 'John' dan peran = ''
"Halo John, Pilih peranmu untuk memulai game!"
 
//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
"Selamat datang di Dunia Werewolf, Jane"
"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"
 
//Output untuk Input nama = 'Jenita' dan peran 'Guard'
"Selamat datang di Dunia Werewolf, Jenita"
"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."
 
//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
"Selamat datang di Dunia Werewolf, Junaedi"
"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!"
*/