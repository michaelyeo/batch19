// release 0

class Animal {
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded= "false";
    }
    get cname (){
        return this.name;
    }
    set cname (x){
        this.name = x;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false